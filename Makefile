default: clean build-server run-server

update-website: clean update copy-website update-website-mod-dump build-client update-website-downloads

copy-website:
	cp -r website/* /var/www/html

clean:
	- rm -rf server-build
	- rm -rf client-build
	- rm -rf client-exports

update:
	git pull

update-website-mod-dump:
	cd moddump; npm i
	cd moddump; node dumpMods.js /var/www/html/index.html

update-website-downloads:
	- rm /var/www/html/downloads/*
	cp -r client-exports/* /var/www/html/downloads

download-server:
	- rm -rf serverFiles
	mkdir serverFiles
	cd serverFiles; wget https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-installer.jar
	cd serverFiles; java -jar forge*installer.jar --installServer
	cd serverFiles; rm forge*installer.jar forge*installer.jar.log

clean:
	- rm -rf server-build
	- rm -rf client-build
	- rm -rf client-exports

build-server:
	mkdir server-build
	cp -r client/* server-build # most files
	cp -r serverFiles/* server-build # server
	cp -r server/* server-build # serverside mods/configs
	cd server-build; xargs -a ../clientside.txt rm -rf # remove clientside-only files
	- cp -r ../untrackedServerFiles/* server-build

run-server:
	cd server-build; java -Xms1G -Xmx4G -Dfml.readTimeout=120 -jar forge-1.12.2*.jar nogui

test-server: clean build-server
	cd server-build; echo "stop" | java -Xms1G -Xmx4G -jar forge-1.12.2*.jar nogui
	grep '\[Server thread/INFO\] \[net.minecraft.server.dedicated.DedicatedServer\]: Done' server-build/logs/latest.log

build-client:
	cp -r client client-build
	cat client/config/defaultoptions/options.txt client/config/defaultoptions/keybindings.txt > client-build/options.txt
	mkdir client-exports
	mkdir client-build/bin
	wget -O client-build/bin/modpack.jar https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-installer.jar
	cd client-build; zip -r ../client-exports/techniclauncher-skyblock.zip *
	rm -rf client-build/bin
	mv client-build nigeriacraft
	cd nigeriacraft; zip -r ../client-exports/nigeriacraft-skyblock.zip *
	mv nigeriacraft client-build
