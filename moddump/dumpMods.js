const https = require("https");
const cheerio = require('cheerio');
const fs = require('fs');
var { mods } = require("../client/mods/.modlist.json");

const $ = cheerio.load(
  fs.readFileSync(process.argv[2], "utf8")
);

(async function() {
  var moddata = [];
  for(var i in mods) {
    var mod = mods[i];
    var url = "https://addons-ecs.forgesvc.net/api/v2/addon/" + mod.project_id;
    var info = await fetchJSON(url, mod.project_id);
    moddata.push(info);
    console.log(i - 0 + 1, "/", mods.length, "fetched", `(${info.name})`);
  }

  var badCat = ["library-api", "mc-miscellaneous", "cosmetic", "map-information", "server-utility"];
  moddata = moddata.sort((a, b) => {
    for(var slug of badCat) {
      if(a.categories.find(cat => cat.slug == slug))
        return 1;
      if(b.categories.find(cat => cat.slug == slug))
        return -1;
    }
  });
  
  while(badCat.length) {
    var slug = badCat.pop();
    moddata.find(info => info.categories.find(cat => cat.slug == slug)).catHeader = `<p style="margin:0;margin-top:.5em;color:white">${slug}:</p>`;
  }
  
  var html = "";
  for(var info of moddata) {
    var thumbUrl = info.attachments.find(att => att.isDefault);
    if(thumbUrl) thumbUrl = thumbUrl.url;
    html += `${info.catHeader || ""}
        <a class="mod-entry" href="${info.websiteUrl}" target="_blank">
            <span class="mod-thumb-wrapper">
                <img class="mod-thumb" src="${thumbUrl}">
            </span>
            <span class="mod-name">${info.name}</span><span class="mod-author">by ${info.authors.map(a=>a.name).join(", ")}</span><br>
            <span class="mod-summary">${info.summary}</span>
        </a>`;
    console.log(i - 0 + 1, "/", mods.length, "html built", `(${info.name})`);
  }

  $(".modlist").first().html(html);
  fs.writeFileSync(process.argv[2], $.root().html());
})();

function fetchJSON(url, cacheKey) {
  var https = require('https');
  cacheKey = "cache/" + cacheKey;
  if(fs.existsSync(cacheKey) && (Date.now() - fs.statSync(cacheKey).mtimeMs) < 1*24*60*60*1000) {
    console.log("Hit!", cacheKey);
    return JSON.parse(fs.readFileSync(cacheKey));
  }

  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      var { statusCode } = res;
      var contentType = res.headers['content-type'];

      let error;

      if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
          `Status Code: ${statusCode}`);
      } else if (!/^application\/json/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
          `Expected application/json but received ${contentType}`);
      }

      if (error) {
        console.error(error.message);
        // consume response data to free up memory
        res.resume();
      }

      res.setEncoding('utf8');
      let rawData = '';

      res.on('data', (chunk) => {
        rawData += chunk;
      });

      res.on('end', () => {
        try {
          fs.writeFileSync(cacheKey, rawData);
          const parsedData = JSON.parse(rawData);
          resolve(parsedData);
        } catch (e) {
          reject(e.message);
        }
      });
    }).on('error', (e) => {
      reject(`Got error: ${e.message}`);
    });
  });
}
