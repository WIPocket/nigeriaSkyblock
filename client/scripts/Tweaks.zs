// vanilla ores
mods.tconstruct.Casting.addBasinRecipe(<minecraft:iron_ore>, <minecraft:iron_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:gold_ore>, <minecraft:gold_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:coal_ore>, <minecraft:coal_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:lapis_ore>, <minecraft:lapis_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:diamond_ore>, <minecraft:diamond_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:redstone_ore>, <minecraft:redstone_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:emerald_ore>, <minecraft:emerald_block>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:quartz_ore>, <minecraft:quartz_block>, <liquid:stone>, 144, true);

// te ores
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:0>, <thermalfoundation:storage:0>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:1>, <thermalfoundation:storage:1>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:2>, <thermalfoundation:storage:2>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:3>, <thermalfoundation:storage:3>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:4>, <thermalfoundation:storage:4>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:5>, <thermalfoundation:storage:5>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:6>, <thermalfoundation:storage:6>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:7>, <thermalfoundation:storage:7>, <liquid:stone>, 144, true);
mods.tconstruct.Casting.addBasinRecipe(<thermalfoundation:ore:8>, <thermalfoundation:storage:8>, <liquid:stone>, 144, true);

// hehe
<agricraft:crop_sticks>.displayName = "Gender Sticks";
<openblocks:vacuum_hopper>.displayName = "V.A. Cum Hopper";
recipes.addShapeless("rock", <minecraft:bed:14>, [<minecraft:bedrock>]);
<minecraft:dragon_egg>.displayName = "Dragon Eggy";

// tinkers blue slime, magma slime
mods.thermalexpansion.InductionSmelter.addRecipe(<tconstruct:edible:1>, <minecraft:slime_ball>, <minecraft:dye:4>, 6000);
mods.thermalexpansion.InductionSmelter.addRecipe(<tconstruct:edible:4>, <minecraft:slime_ball>, <minecraft:blaze_powder>, 6000);

// inscriber press
recipes.addShaped(<appliedenergistics2:material:19>,
	[
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>]
	]
);
recipes.addShaped(<appliedenergistics2:material:13>,
	[
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <minecraft:gold_ingot>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>]
	]
);
recipes.addShaped(<appliedenergistics2:material:14>,
	[
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <minecraft:diamond>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>]
	]
);
recipes.addShaped(<appliedenergistics2:material:15>,
	[
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <appliedenergistics2:material>, <ore:itemSilicon>],
		[<ore:itemSilicon>, <ore:itemSilicon>, <ore:itemSilicon>]
	]
);

// aa storage crates
// "small" crate
recipes.removeByRecipeName("actuallyadditions:recipes162");
mods.thermalexpansion.InductionSmelter.addRecipe(<actuallyadditions:block_giant_chest>, <ironchest:iron_chest:2>, <actuallyadditions:item_crystal_empowered:2> * 4, 6000);
// medium crate
recipes.removeByRecipeName("actuallyadditions:giant_chest_media");
mods.thermalexpansion.InductionSmelter.addRecipe(<actuallyadditions:block_giant_chest_medium>, <actuallyadditions:block_giant_chest>, <actuallyadditions:item_crystal_empowered:2> * 10, 6000);
// large crate
recipes.removeByRecipeName("actuallyadditions:giant_chest_large");
mods.thermalexpansion.InductionSmelter.addRecipe(<actuallyadditions:block_giant_chest_large>, <actuallyadditions:block_giant_chest_medium>, <actuallyadditions:block_crystal_empowered:2> * 2, 6000);

// dank null 5
recipes.removeByRecipeName("danknull:dank_null_5");
recipes.removeByRecipeName("danknull:diamondtoemerald");
recipes.addShaped(<danknull:dank_null_5>, 
  [
    [<actuallyadditions:block_giant_chest_large>, <danknull:dank_null_panel_5>, <actuallyadditions:block_giant_chest_large>],
    [<danknull:dank_null_panel_5>, <danknull:dank_null_4>, <danknull:dank_null_panel_5>],
    [<actuallyadditions:block_giant_chest_large>, <danknull:dank_null_panel_5>, <actuallyadditions:block_giant_chest_large>]
  ]
);
